// Test for measuring the position of a servo MG996R on a nucleo_f411re
// This test implies the following input/ouput
//
// input :
//   -1 analog input: A0
//
// output:
//   - none
//
// Author : Bechir TABIA

#include "mbed.h"
#define MINVOLT 3.0f
#define MAXVOLT 95.0f
#define MINPOS  0.0f
#define MAXPOS  180.0f

AnalogIn position(A0);
DigitalOut led(LED1);
Serial pc(USBTX, USBRX);

void init(void){
  pc.baud(115200);
}
// get real position from
float getPosition(float voltage){
  float X = trunc(voltage * 100);
  float A = (MAXPOS - MINPOS) / (MAXVOLT - MINVOLT);
  float B = MINPOS - (A *  MINVOLT);
  return (A * X) + B;
}

void loop(void){
  float voltage = trunc(position.read() * 100);
  float pos = getPosition(position.read());
  pc.printf("Measured voltage: %f\n", voltage);
  pc.printf("Measured position: %f\n", pos);
  wait(0.1);
}




int main (void)
{
  init();
  while(1){
    loop();
  }
}
